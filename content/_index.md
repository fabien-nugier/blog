### Welcome!

Welcome to my blog! This is a place where I publish content on machine learning, on my
readings and my free-time activities.

The topics which I am mostly interested about relate to physics, investments,
infrastructures and climate, so you can expect to see more posts applying machine learning
to these fields of research and engineering.

I hope you will find the content interesting. So do not hesitate to jump directly
to my [Blog Posts](https://fabien-nugier.gitlab.io/blog/post/)! :)


### Acknowledgments

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and uses the `beautifulhugo` theme to support the content.
Head over to the [GitLab project](https://gitlab.com/pages/hugo) to start your own!

If you want to learn more about me, please visit the
[about me](https://fabien_nugier.gitlab.io/hugo-blog/page/about/) page.

 