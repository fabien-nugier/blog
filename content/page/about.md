---
title: About Me
subtitle: My background, my interests and projects.
comments: false
---

My name is Fabien Nugier. I am a French guy in his 30s, nice to meet you.

### My background

I have a PhD in theoretical physics from the University Pierre
et Marie Curie, which is now belonging to Sorbonne University.
I have worked at different research institutions that
include the Ecole Normale Supérieure, the University of Bologna and the
National Taiwan University.

### My interests

I am passionate about several topics which include in chronological order:
physics (especially astrophysics and cosmology), machine learning and AI,
sustainable finance (investments, infrastructures, ESG) and climate research
(climate predictions, risks, geospatial analysis).


### My projects

At the moment I work on diverse topics involving infrastructures, graph neural
networks and geospatial analysis.





