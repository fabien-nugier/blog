---
title: The physics of croissants
subtitle: From croissants to the light
date: 1921-10-19
tags: ["fun facts", "energy", "consumptions"]
---

[**Date:** 2021-10-19]  

| ![A typical French breakfast](https://fabien-nugier.gitlab.io/blog/post/blog5/croissant_coffee.jpg) |
|:--:|
| <b>A typical French breakfast.</b>|

Since my lasts posts were quite long and addressing the pretty worrying topic of climate
change, I will try to bring a lighter note today with a fun equivalence of energy which
will be understandable by many people, especially French, and that I will call the
"physics of croissants".

### A little story

Let me explain how the idea of this post came to my thoughts, mainly for three reasons.
First, a few days ago, when writing my second post on [Jean-Marc Jancovici at Mines Paris Tech, 2019](https://fabien-nugier.gitlab.io/blog/post/2021-10-17-blog4-jancovici-course34/),
I had to do a small calculation to verify the average radiative flux at the surface of the
Earth. This radiative flux is about 238 W / m^2, corresponding to a flux of 1361 W / m^2
averaged over latitudes and half longitudes (dividing factor of 2), over the rotation
of the Earth (the two sides with respect to the Sun, i.e. another dividing factor of 2)
and considering a 30% average reflexion from clouds and snow (albedo), c.f. _Note 2_ there.
Second, like many people do, yesterday evening I went running and after my 15 km run
reviewed my watch, noticing that it was indicating a bit more than a thousand Calories
(with a capital 'C', since it is important) burnt, I was wondering how much food intake
that really represents... Third, this morning I took my breakfast -- a coffee and two 
croissants -- like many of us French do sometimes.

Since my run was still fresh in my mind, I could not help wondering if my breakfast was
not simply cancelling all my efforts of yesterday, and I wondered how many Watts this would
correspond to. Thinking a little about it, it seemed to me that it would be more interesting
in the end to relate that to a quantity that does not move much over time, and which is
universal in the sense that it relates to everything growing on Earth, namely the time
of sunshine that a square meter receives.


### Solving the problem

Now the little story has been told, let us formulate the question more precisely:

_How much time do I need to wait to receive as much energy on a meter squared on Earth
from sunlight, averaging over the latitudes + Earth rotation + albedo (rough estimates), to
build up the same amount of calories that my croissant contains?_

First, let us recall the following equivalences:
- 1 Calorie = 1 kcal (hence the capital 'C' being important),
- 1 Watt (W) = 1 J  s^(-1) (a Joule per second),
- 1 cal = 4.184 J ≈ 1.162  10^(-6) kWh (using the thermochemical calorie [definition](https://en.wikipedia.org/wiki/Calorie)),
- the reverse gives 1 kJ = 0.239 kcal.

Second, a simple google search gives us the painful reality that a croissant contains
a nutritional energy of about 300 Calories! (obviously, there is no international standards
for croissants, some are smaller than others, and some are more buttery).

Based on this average, we get:

$$300 ~ Calories = 300 \cdot 10^3 ~[cal] \times 1.162 \cdot 10^{-6} ~[kWh \cdot cal^{-1}] = 0.349 ~kWh$$

Considering the radiative flux of 238 W m^(-2) described above, follows the
equivalence for a m^2:

$$300 ~Calories / m^2 = \frac{0.349 ~[kWh]}{0.238 ~[kW ~ m^{-2}]} \frac{1}{[m^2]} \simeq 1.46 ~hours.$$

In other words, the nutritional energy of a croissant corresponds to about 1.5 hours of
sunshine on a meter squared at the surface of the Earth! To me, it is a big surprise.

The same calculation can be done with other types of food, using the following coefficient
of conversion:

**1 Calorie = 4.88 x 10^{-3} average hours of sunlight on a 1 m^2 surface of the Earth.**

It is interesting to recall that a woman needs on average 2000 Calories per day to live,
and a man about 2,500 Calories. These quantities correspond respectively to 9.7 hours and
12.2 hours of sunlight, a big chunk of our day! Obviously, doing some gardening would
teach us that a human needs more than a meter squared of land to cover its nutritional
needs. That is because not all solar radiation goes into the production of plants.

A simple calculation also gives us:

$$1 ~kWh \simeq 860 ~Calorie$$

Since this post is supposed to be light and fun, I will not present the equivalent in
hours of radiation needed to cover the year consumption of a French citizen, I will just
recall that his/her consumption for a year is about 50,000 kWh :).

Let us finally recall a statement made in the first blog post on [Jean-Marc Jancovici at Mines Paris Tech, 2019](https://fabien-nugier.gitlab.io/blog/post/2021-10-13-blog3-jancovici-course12/).
In that post we mentioned that the power of a pair of human legs is about 100 W. It is
easy to verify from the equivalence above that a 100 Wh corresponds to 86 Calories.
Multiplied by 24, we get 2064 Calories during a day, very close to the caloric needs
of men and women stated above. As you can imagine though, this normal intake would not
allow us to use our legs for a whole day without eating much more! This is probably what
we need for a few hours of exercise in a day. So where is the mismatch?
Well, the mismatch comes from the fact that the human body does not fully convert the
nutritional energy of food into work. Indeed, the human body as an energy conversion efficiency
of about 25% (see this [interesting blog post](https://dothemath.ucsd.edu/2011/11/mpg-of-a-human/)),
which is higher than cars (≈ 20%), which roughly explains our use of legs for few hours
(let say 6 hours) with an intake of about 2000 Calories during a day.


### Conclusions

This post was mainly intended to be fun. Hopefully it was.

Nevertheless, I think it reveals pretty clearly how much energy we need to just function,
whatever our mode of consumption. Since Calories are not that easy to "intuitate" (at
least for me), I find the equivalent of sunlight on a square meter of land much more 
practical.

Furthermore, it is interesting to notice that here was only considered the
chemical energy delivered by food itself, but its production often requires much more
energy through the growth of ingredients, use of machines, transportations, etc.

One can easily use this equivalence to evaluate our impact on the Earth through a
conversion in time (and maybe in land size). The advantage of such a comparison is that
it relates a daily individual scale (food intakes, human needs), to a global quantity
which is shared by everybody in the same way (average sunlight and time).

