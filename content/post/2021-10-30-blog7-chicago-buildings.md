---
title: Chicago's buildings
subtitle: Using GeoPandas on a real use case
date: 1921-10-31
tags: ["data science", "maps", "geospatial", "python"]
---

[**Date:** 2021-10-31]

![Chicago city](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_city.jpg)

Chicago is a vibrant city which ranks as the third most populous city in the United States
of America, hosting more than 2.7 million inhabitants (c.f. its [Wiki page](https://en.wikipedia.org/wiki/Chicago)).
Obviously, such a big city requires massive infrastructures to support the needs of the
population, and every city in the world has developed its infrastructure according to
historical and social influences, economic conjunctures and well as political views
regarding its future.

In this post, I propose to leverage the amazing public data made accessible by the
[Chicago Data Portal](https://data.cityofchicago.org/Buildings/Building-Footprints-current-/hz9b-7nh8),
and to see what can be said from such kind of data. There is a lot of information that can
be extracted from such high quality dataset, and it is an occasion for me here to apply
the knowledge of GeoPandas presented in my [last post](https://fabien-nugier.gitlab.io/blog/post/2021-10-29-blog6-geopandas/).

Hence, in this post I will draw some maps and focus on features that appear the most
relevant to me. I will draw map in order to show so historical as well as spatial
specificities of the buildings of Chicago, and will show that once again graphs are
the perfect tool to analyse complex relationships such as co-occurrence of words building
descriptions.

Without further ado, let us draw some nice maps of Chicago!


# Loading and selecting the data

The initial dataset provided by the Chicago Data Portal is very close to 1 Gb in size,
contains 43 features and 820606 buildings. In order to deal with plots faster and make
maps focusing on the city center, we start by reducing the window of the buildings that
the dataset contains.

Stepping towards that goal, we load the following packages:
```python
# Standard packages imports
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import cm
import seaborn as sns
import itertools

# For dataset
import fsspec

# GeoPandas
import geopandas as gpd
gpd.options.display_precision = 3
from shapely.geometry import Polygon

# Folium
import folium
```
We then load the original dataset:
```python
# Loading the original dataset
path = './blog7/chicago_building_footprints.geojson'
chicago_all = gpd.read_file(path)
```
and load it again, but applying a selection window:
```python
# Loading and restricting buildings to those inside the box
path = './blog7/chicago_building_footprints.geojson'
bbox = (-87.775, 41.82, -87.600, 41.92)
chicago_selection = gpd.read_file(path, bbox=bbox)
```
This new dataset has 158391 buildings, which makes it easier to handle.

Making a rectangle polygon from the selection window coordinates, we can plot both
original dataset and the selected one as follows:
```python
# Make a polygon with the box dimensions
selection = Polygon(((-87.775, 41.82), (-87.600, 41.82), (-87.600, 41.92), (-87.775, 41.92)))

# Turn it into a GeoDataFrame
selection_gdf = gpd.GeoDataFrame(data={'name': 'selection', 'geometry': selection}, index=[0],
                                 geometry='geometry', crs=chicago_selection.crs)

# Plot
fig, ax = plt.subplots(figsize=(10, 10))
chicago_all.plot(ax=ax, color='gray')
selection_gdf.plot(ax=ax, edgecolor='red', lw=3, alpha=0.1)
plt.title("Chicago Data Portal building's footprint and selected window")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_data_selection.jpg)

Special buildings, assuming they are special because they have a name in the dataset
columns 'bldg_name1' and/or 'bldg_name2', are selected in the following way:
```python
# Replace '' values by NaN values
chicago_all['bldg_name1'].replace(to_replace='', value=np.nan, inplace=True)
chicago_all['bldg_name2'].replace(to_replace='', value=np.nan, inplace=True)

# Create a mask
mask1 = [x == x for x in chicago_all['bldg_name1']]
mask2 = [x == x for x in chicago_all['bldg_name2']]
mask = mask1 or mask2

# Restrict the dataset with that mask
chicago_only_named_bldgs = chicago_all[mask]

# Plot
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago_only_named_bldgs.plot(ax=ax, color='black')
plt.title("Chicago Data Portal special buildings")
plt.show()

# Save the dataset
path = './blog7/new_chicago_only_named_bldgs.geojson'
chicago_only_named_bldgs.to_file(path, driver='GeoJSON')
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_all_special_buildings.jpg)

Having these two restricted datasets `chicago_selection` and `chicago_only_named_bldgs` at
our disposal, we can now start to explore their content and extract some knowledge about
the city.


# Exploring the selected area

Let us reload our saved dataset `chicago_selection` under the simpler name `chicago`:
```python
path = './blog7/new_chicago_buildings_selection.geojson'
with fsspec.open(path) as file:
    chicago = gpd.read_file(file)
```

The dataset we are dealing with here is complex and contains many features, as described in
the related [list of attributes](https://data.cityofchicago.org/api/assets/003C600C-3A66-4605-8E7E-2477AAE95E16).
As any realistic dataset, it also contains missing data. Going through each of the
features, excluding those who are for "Internal Use Only" as well as some of the "not
actively maintained" or those with no interest at the moment, we end up with a
dataset containing only 16 features. We also convert some data to numerical values
and extract the number of unique values in each feature. All is done as follows:
```python
# Features to delete
to_delete = ['no_stories', 'suf_dir1', 'x_coord', 'y_coord',
             'cdb_city_i', 'y_coord', 'unit_name', 'qc_date', 'edit_date',
             'label_hous', 'st_name1', 't_add1', 'z_coord', 'qc_userid',
             'orig_bldg_', 'footprint_', 'condition_', 'bldg_activ', 'edit_useri',
             'edit_sourc', 'f_add1', 'vacancy_st', 'bldg_creat', 'no_of_unit', 
             'create_use', 'demolished', 'pre_dir1', 'harris_str']

# Delete them
chicago.drop(columns=to_delete, inplace=True)

# Change some numerical features
features_to_convert = ['shape_area', 'year_built', 'stories', 'bldg_sq_fo', 'shape_len']
for feature in features_to_convert:
    chicago[feature] = chicago[feature].astype(float)
chicago['year_built'] = chicago['year_built'].replace(to_replace=0.0, value=np.nan)

# Show remaining features and number of unique values
nbr_uniques = chicago.nunique()
nbr_uniques
```
```
shape_area     50423
year_built       162
stories           70
non_standa         5
qc_source          3
st_type1          16
bldg_statu         3
bldg_name2       165
bldg_sq_fo      5593
bldg_end_d         2
bldg_id       158391
bldg_condi         6
bldg_name1      1375
comments         543
shape_len     112802
geometry      158391
dtype: int64
```

Let us now look into these different features and present the most interesting ones.
In order to save space and focus the discussion on data interpretation, some pieces
of code will sometimes be skipped over or explained in simple words.

We can represent the buildings of the dataset from their year of build:
```python
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago.plot(column='year_built', ax=ax, cmap='rainbow', categorical=False,
             legend=True, legend_kwds={'label': "Year built", 'orientation': "horizontal"})
plt.title('Year Built for buildings in Chicago')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_year_built.jpg)

As we can see, large buildings tend to be new buildings, which can be explained by the
improvement in constructions techniques as well as a growing need for large infrastructures
in modern economy. We could also think by looking at the map that new buildings tend to be close
to empty spaces (some of which representing parks and large roads). However, sampling over 200
buildings and counting the number of buildings they have within a radius of 500 meters, we find that
there does not seem to be a correlation between age and empty spaces. Bigger samples could be tested,
but a large number of buildings is very expensive computationally.

On the other hand, one does see a correlation between year built and very large buildings
constructions. Using the 'shape_area' feature in the data, one can see this evolution of
buildings maximal size over time from the following plot:
```python
plt.figure(figsize=(10, 5))
chicago.plot.scatter(x='year_built', y='shape_area', alpha=0.3)
plt.title('Area of buildings in terms of their year built')
plt.xlabel('Year built')
plt.ylabel('Building area (in square feet)')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/year_built_shape_area.jpg)

One other interesting fact that we can mention is that since 56.3% of buildings have a built
date, we can trust the clear patterns of constructions started 150 years ago.
Indeed we obtain the following histogram easily:
![](https://fabien-nugier.gitlab.io/blog/post/blog7/year_built_histo.jpg)

As visible, constructions were extremely frequent at the beginning of the 20th century,
probably in large part due to the so-called [First Great Migration](https://en.wikipedia.org/wiki/Great_Migration_(African_American))
(1210-1940) when American from African ancestors moved to the Northern cities to find
job opportunities in the industrial sector. The Great Depression had a killing effect
on constructions during the 30s and the WWII efforts very likely imposed restrictions
on constructions in the 40s. It is only in the 1950s and 60s that constructions reemerged in
Chicago, probably also relating to the [Second Great Migration](https://en.wikipedia.org/wiki/Great_Migration_(African_American)).
Finally, constructions had a recent boom after year 2000, for a reason that I ignore.

Let us add that we are dealing here with a map whose scope is quite close to the city center.
In the USA, cities have a typical structure that, for historical reasons, tend to put
low income citizens closer to the center (c.f. this [video](https://www.youtube.com/watch?v=aQSxPzafO_k&t=170s)
explaining very interesting facts). This specificity in the localization could influence the
amount of constructions represented here during some periods of history.


Focusing on the 'stories' feature, we get the following plot:
```python
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago.plot(column='stories', ax=ax, cmap='Reds', categorical=False,
             legend=True, legend_kwds={'label': "Number of stories", 'orientation': "horizontal"})
plt.title('Year Built for buildings in Chicago')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_stories.jpg)
This map clearly shows that Chicago's central business district (CBD) is situated on the East,
along Lake Michigan.

We can also check the building conditions:
```python
chicago['bldg_condi'].replace(to_replace='', value=np.nan, inplace=True)

fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago.plot(column='bldg_condi', ax=ax, cmap='Accent', categorical=True,
             legend=True, legend_kwds={'bbox_to_anchor': (1.3, 1)})
plt.title('Chicago buildings by their condition.')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_condition.jpg)

The GeoDataFrame has two features named 'bldg_name1' and 'bldg_name2' which give some
denomination to the buildings. Sometimes, these features have the same name, sometimes others,
sometimes an address or even pretty different names. We will explore the content of these
features in the next section, but let us show a map with the special buildings in our
selection area with some arbitrary colors:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_special_buildings_in_selection.jpg)

To make it more fun, let us use the [Seaborn package](https://seaborn.pydata.org/) and
do a kernel density estimate on top of the centroids of these buildings. This leads to
the following map, in which I have added a scale bar:
```python
from matplotlib_scalebar.scalebar import ScaleBar

mask1 = [x == x for x in chicago['bldg_name1']]
mask2 = [x == x for x in chicago['bldg_name2']]
mask = mask1 or mask2

df = chicago[mask].copy()
df['centroid'] = df.geometry.to_crs('+proj=cea').centroid.to_crs(32619)
df.set_geometry(col='centroid', inplace=True)

longitudes = [point.xy[0][0] for point in df['centroid']]
latitudes = [point.xy[1][0] for point in df['centroid']]

fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago.to_crs(32619).plot(ax=ax, color='gray')
chicago.to_crs(32619)[mask].plot(ax=ax, color='red')
df.to_crs(32619).plot(ax=ax, color='black', alpha=0.5, markersize=5)
sns.kdeplot(longitudes, latitudes, shade=True, cmap='Oranges', ax=ax, alpha=0.7)
ax.add_artist(ScaleBar(dx=1))
plt.title('Chicago special buildings.')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_famous_buildings_density2.jpg)

If we assume that the density of special buildings (i.e. buildings with a name) is a
good proxy for estimating the location of a city center, then this map clearly shows
a city center around (long, lat) = (-87.625, 41.89), and this corresponds to Chicago's
business center.

Before we enter the diversity of special buildings, let us focus on a single type of
special buildings found in features 'bldg_name1' and 'bldg_name2', namely hospitals.
We can select the hospitals going through the following code lines:
```python
# Make a list of all names in the selection datatset
names_list1 = chicago['bldg_name1'].unique().tolist()
names_list2 = chicago['bldg_name2'].unique().tolist()
names_list = names_list1 + names_list2
names_list.remove(np.nan)

# Create list of names including 'HOSPITAL'
hospitals = [x for x in name_list if 'HOSPITAL' in str(x)]

# Create a mask to get these names
mask1 = [(x in hospitals) for x in chicago['bldg_name1']]
mask2 = [(x in hospitals) for x in chicago['bldg_name2']]
mask = mask1 or mask2

# Define a dataset with selected hospital buildings and buffer of 1km around
df = chicago[mask].copy()
radius_1km = 1000. # in meters
df['buffer'] = df.geometry.to_crs('+proj=cea').buffer(distance=radius_1km).to_crs(df.crs)
df.set_geometry(col='buffer', inplace=True)

# Plot the map
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
df.plot(ax=ax, color='yellow', alpha=0.3)
chicago.plot(ax=ax, color='gray')
chicago[mask].plot(ax=ax, color='red')
plt.title('Chicago hospitals with a 1 km radius circle.')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_hospitals_circles.jpg)

Despite the slight deformation of circles due to the vertical stretch of the map, we can
see that hospitals are pretty well scattered over the selected area. Similar maps can
be done with other types of buildings, e.g. by showing churches, police stations, gas stations,
universities and schools, etc. We will come back to these special buildings in the next
section, taking the whole data available and plotting many categories of buildings together.

We can also make a more impressive map by computing the distance between each building
and the nearest hospital within our selection box, and then use this distance as the
base for coloring buildings on the map. This can be done as follows:
```python
from tqdm import tqdm

# Compute the centroids of all buildings
chicago.set_geometry(col='geometry', inplace=True)
chicago['centroid'] = chicago.geometry.to_crs('+proj=cea').centroid.to_crs(chicago.crs)

# Create a new feature with distance to nearest hospital
radius_earth = 6371. # km
chicago['dist_nearest_hospital'] = np.nan

for idx1, row1 in tqdm(chicago.iterrows()):
    
    # Get the current building's centroid
    c1 = row1['centroid']
    
    # Compute the minimal distance from it to hospitals
    min_dist = 1E10
    for idx2, row2 in df.iterrows():
        
        # Get the hospital centroid
        c2 = row2['centroid']
        # Compute the distance, save minimal
        dist = c1.distance(c2)
        
        if dist < min_dist:
            min_dist = dist
            
    # Affect that value to the frame
    chicago.loc[idx1, 'dist_nearest_hospital'] = min_dist * (np.pi / 180) * radius_earth

# Plot the map
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
chicago.plot(column='dist_nearest_hospital', ax=ax, cmap=cm.RdYlGn_r, categorical=False,
             legend=True, legend_kwds={'label': "Distance to nearest hospital (in km)",
                                       'orientation': "horizontal"})
plt.title("Chicago's buildings' distance to hospitals.")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_nearest_hospital.jpg)

This coloring clearly reveals the buildings which are farther from hospitals. One should
be aware however that buildings on the side of the map are probably biased since they
are "aware" of only half of their surrounding buildings (or even a quarter when located
in the corners). Hence, it is pretty likely that extending the map farther would change
the colors on the sides of the map, and thus colors can be trusted around the center only.

In addition to hospitals, we can study the scattering of schools and colleges. Not showing
the code considering that it is a little messy to design such a plot, we get the following
representation:
![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_univ_coll_scho_libr.jpg)
It is interesting to notice the concentration of universities on the Lake Michigan shore
as well as around the [University of Illinois Chicago (UIC)](https://en.wikipedia.org/wiki/University_of_Illinois_Chicago).


# Exploring special buildings

Now that we have presented results regarding our selected area, let us explore the
data based on special buildings over the whole available dataset.

This dataset, previously contained in the GeoDataFrame `chicago_only_named_bldgs`, is
not reloaded under the name `all_specials`:
```python
path = './blog7/new_chicago_only_named_bldgs.geojson'
with fsspec.open(path) as file:
all_specials = gpd.read_file(file)
```

Let us plot a Folium map to help us visualize the data:
```python
all_specials['bldg_name'] = np.nan
for idx, row in all_specials.iterrows():
    all_specials.loc[idx, 'bldg_name'] = str(row['bldg_name1']) + ' + ' + str(row['bldg_name2'])

all_specials.explore(column='bldg_name', legend=False)
```
| ![](https://fabien-nugier.gitlab.io/blog/post/blog7/map_folium_special_buildings.jpg) |
|:--:|
| <b>Interactive map [HERE](https://fabien-nugier.gitlab.io/blog/post/blog7/folium_map_special_buildings_name.html)</b>|
<br>

We first create a dictionary containing the number of occurrences of each word in the
features 'bldg_name1' and 'bldg_name2', counting the word only one time for each row
of the dataset. We also create a dictionary that counts the number of times two
words appear in the same row (meaning in both 'bldg_name1' and 'bldg_name2').
This is done as follows:
```python
occ_dict = {}
rel_dict = {}

# Go through all rows of the dataset
for idx in tqdm(all_specials.index):

    # Import sentences
    r1 = all_specials.loc[idx, 'bldg_name1']
    r2 = all_specials.loc[idx, 'bldg_name2']
    
    # Build a set of unique words appearing on this row
    if r1 is not None and r2 is None:
        r = r1.split(' ')
    elif r1 is None and r2 is not None:
        r = r2.split(' ')
    elif r1 is not None and r2 is not None:
        r = r1.split(' ') + r2.split(' ')
    r = list(set(r))

    # Randomize r:
    np.random.shuffle(r)
    
    # Add these words to the occurrence dictionary
    for word in r:
        if word not in occ_dict.keys():
            occ_dict[word] = 1
        else:
            occ_dict[word] += 1
            
    # Add these words to the relationship dictionary
    for word1 in r:
        for word2 in r:
            pair = (word1, word2)
            if pair not in rel_dict.keys():
                rel_dict[pair] = 1
            else:
                rel_dict[pair] += 1
```
We also derive quantities from these, proceeding as follows:
```python
# Make the list of words
words = list(occ_dict.keys())

# Get the number of occurrences (which is the nodes sizes)
n = [occ_dict[word] for word in words]

# Build the overlap matrix (which is a weight matrix)
w = np.zeros(shape=(len(words), len(words)))
for idx1, word1 in enumerate(words):
    for idx2, word2 in enumerate(words):
        pair = (word1, word2)
        if pair in rel_dict.keys():
            w[idx1, idx2] = rel_dict[pair]
```

The code above generating way to many words (3289 to be precise), and many of them
do not make enough sense to be considered as they contain numbers, small words, or
even names of locations or people. So I wrote the following function to recompute
important quantities from a restricted list called `to_keep` and plot a graph of these words:
```python
def compute_restricted_graph(words, occ_dict, rel_dict, to_keep, file1, file2):
    """
    Computes the restricted quantities and associated graph.
    
    Args:
        words (list): list of words we will take words from.
        occ_dict (dict): dictionary of words occurrence.
        rel_dict (dict): dictionary of pairs of words relationship (co-occurrence).
        file1 (str): name of the first file to save the weight matrix heatmap.
        file2 (str): name of the second file to save the graph.
        
    Returns:
        n_rest (list): list of occurrences of restricted list of words.
        w_rest (np.ndarray): overlapping of words (co-occurrence).
        occ_dict_rest (dict): dictionary of restricted list's words occurrence.
        rel_dict_rest (dict): dictionary of restricted list's words relationships.
        G (networkx.classes.graph.Graph): graph of the restricted list of words.
    """

    # New list of words
    words_rest = []
    for word in words:
        if word in to_keep:
            words_rest.append(word)

    # New node sizes
    n_rest = []
    for word in words_rest:
        n_rest.append(occ_dict[word])

    # New weight matrix
    w_rest = np.zeros(shape=(len(words_rest), len(words_rest)))
    for idx1, word1 in enumerate(words_rest):
        for idx2, word2 in enumerate(words_rest):
            pair = (word1, word2)
            if pair in rel_dict.keys():
                w_rest[idx1, idx2] = rel_dict[pair]

    # New occ_dict:
    occ_dict_rest = {}
    for word in words_rest:
        occ_dict_rest[word] = occ_dict[word]

    # New rel_dict:
    rel_dict_rest = {}
    for pair in rel_dict.keys():
        if pair[0] in words_rest and pair[1] in words_rest:
            rel_dict_rest[pair] = rel_dict[pair]

    # rel_dict_rest as a list:
    rel_dict_rest_list = []
    for pair in rel_dict_rest.keys():
        # Do not keep the diagonal terms
        if pair[0] != pair[1]:
            rel_dict_rest_list.append((pair[0], pair[1], rel_dict_rest[pair]))
            
    # Plot the weight matrix
    sns.heatmap(w_rest, cmap='rainbow')
    plt.savefig(file1, bbox_inches='tight')
    plt.title('Overlap between words after restriction')
    plt.show()
    
    # Check that its symmetric
    tol=1e-8
    is_sym = lambda A: np.linalg.norm(A-A.T, np.Inf) < tol
    symmetric = is_sym(w_rest)
    if symmetric:
        print('w_rest is a symmetric matrix!\n')
    else:
        print('w_rest is not a symmetric matrix!\n')
        assert 0
    
    # Show the content of non-diagonal elements in w_rest
    pct_thres = 0.2
    save_overlaps = []
    for idx1, word1 in enumerate(words_rest):
        for idx2 in range(idx1+1, len(words_rest)):
            word2 = words_rest[idx2]
            pair = (word1, word2)
            if pair in rel_dict_rest.keys():
                rel_strength = rel_dict_rest[(word1, word2)]
                pct_overlap = rel_strength / max(occ_dict[word1], occ_dict[word2])
                save_overlaps.append((word1, word2, rel_strength, pct_overlap))
                if pct_overlap > pct_thres:
                    print(word1, '\t -- ', 100 * round(pct_overlap, 3), '% -- \t', word2)

    # Create Graph
    G = nx.Graph()
    G.add_nodes_from(words_rest)

    # Deal with edges
    factor = 20. / max([rel[2] for rel in rel_dict_rest_list])
    edges_width = [factor * rel[2] for rel in rel_dict_rest_list]
    G.add_weighted_edges_from(rel_dict_rest_list)

    # Deal with nodes
    factor = 100
    nodes_size = [x * factor for x in n_rest]

    # Deal with layout
    # pos = nx.kamada_kawai_layout(G)
    # pos = nx.spring_layout(G)
    pos = nx.shell_layout(G)

    # Plot graph
    plt.figure(figsize=(70, 70))
    nx.draw(G, pos=pos, with_labels=True, font_weight='bold', font_size=30,
            node_size=nodes_size, width=edges_width, alpha=0.5)
    plt.savefig(file2, bbox_inches='tight')
    plt.show()
    
    return n_rest, w_rest, occ_dict_rest, rel_dict_rest, G
```
and the following function to plot the overlapping on an even smaller list of words,
typically applied to the restriction of the most frequent words in the list we are dealing with:
```python
def get_overlap_matrix(list_words, rel_dict, filename):
    """
    Generates the overlap matrix for a list of words and their
    corresponding relationship dictionary.
    """

    # Check
    set1 = set(list_words)
    set2 = set(list([rel[0] for rel in rel_dict.keys()]))
    assert set1.issubset(set2)
    
    # Compute the matrix
    matrix = np.zeros(shape=(len(list_words), len(list_words)))
    for idx1, word1 in enumerate(list_words):
        for idx2, word2 in enumerate(list_words):
            pair = (word1, word2)
            if word1 != word2 and pair in rel_dict.keys():
                matrix[idx1, idx2] = rel_dict[pair]

    # Plot the matrix
    plt.figure(figsize=(10, 10))
    sns.heatmap(data=matrix, cmap='Reds', cbar=False, annot=True, fmt='g',
                xticklabels=list_words, yticklabels=list_words)
    plt.title('Overlapping between most frequent selected words.')
    plt.savefig(filename, bbox_inches='tight')
    plt.show()
```


**_Let us skip over the details now and present the results directly._**

We first consider the following (restricted list `to_keep`) of words:
```
['PARK', 'AIRPORT', 'OHARE', 'STRUCTURE', 'SCHOOL', 'STEEL', 'UNIVERSITY', 'STATION',
'PUBLIC', 'PLANT', 'REPUBLIC', 'FIRE', 'LIBRARY', 'CHURCH', 'OFFICE', 'HOSPITAL', 'UIC',
'APARTMENTS', 'TOWER', 'POST', 'CORP', 'PLAZA', 'INC', 'COLLEGE', 'HOTEL', 'PARKING',
'FACILITY', 'CTA', 'MEDICAL', 'LAKE', 'GAS', 'CEMETERY', 'SHORE', 'STRUCTURES', 'IIT',
'POLICE', 'PLACE', 'CARBON', 'FILLING', 'LAKES', 'BANK', 'STATE', 'PUMP', 'CHEMICAL',
'FIELD', 'GARAGE', 'RVS', 'COMMONWEALTH', 'CLUB', 'INSTITUTE', 'WATER', 'CPS',
'AUTO', 'CAMPUS', 'DRIVE', 'SHOP', 'SQUARE', 'FAA', 'MAINTENANCE', 'BRANCH', 'MUSEUM',
'HEALTH', 'RESIDENCE', 'TOWERS', 'UNIV', 'FUEL', 'PUMPING', 'RIVER', 'SHELTER', 'COLLEGES',
'THEATER', 'REPAIR', 'SERVICE', 'SCIENCE', 'COURT', 'FEDERAL', 'SUITES', 'UNION', 'CORPORATE',
'CONCOURSE', 'TERMINAL', 'CATHOLIC', 'COMMUNITY', 'STORAGE', 'TERRACE', 'HOUSING', 'SERVICES',
'AUTHORITY', 'SANITATION', 'COMPANY', 'ADMINISTRATION', 'SHOPPING', 'ACADEMY', 'STREETS', 'LOFTS',
'COOK', 'RESEARCH', 'FARM', 'AIR', 'BAPTIST', 'CAR', 'CONDOMINIUMS', 'ART', 'INN',
'WAREHOUSE', 'ATHLETIC', 'RESTAURANT', 'MEMORIAL', 'COVENANT', 'SUBSTATION', 'CARGO', 'ANNEX',
'INTERNATIONAL', 'TRAINING', 'NWU', 'HOLY', 'OUR', 'LOCALIZER', 'POWER', 'COMMONS', 'LAKESHORE',
'LAKEVIEW', 'LAW', 'FILTRATION', 'AIRLINES', 'TRANSIT', 'GARDENS', 'SCIENCES', 'GARDEN', 'VEHICLE',
'PEOPLES', 'OFFICES', 'OPERATIONS', 'RESIDENCES', 'CONDOS', 'AUDITORIUM', 'ARFF', 'HANGAR', 'CONVENT',
'CHAPEL', 'METRA', 'SUPPLY', 'TECH', 'PARISH', 'LECTURE', 'ATS', 'TRUST', 'PRIVATE', 'GROUND',
'STATION/STORE', 'MISC', 'CENTRAL', 'WRIGHT', 'RENTAL', 'TEMPLE']
```

Proceeding with this list, we find that a few words in this list are correlated, some
strongly suspected to be synonymous. Indeed, 'AIRPORT' and 'OHARE' have an overlap of 99%!
This is because Chicago's main airport is known as the Ohare Airport. Also, some words exist
in their singular and plural forms. The graph that we obtain is the following:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/graph_shell_rest1.jpg)

**Notes:** It turns out that such a graph is known as a [co-occurrence network](https://en.wikipedia.org/wiki/Co-occurrence_network)
and it appears regularly in text mining, entities relationships and concepts networks.

The 20 most frequent words (corresponding to the biggest nodes) are the following:
```
[('AIRPORT', 303),
 ('OHARE', 300),
 ('PARK', 294),
 ('STRUCTURE', 264),
 ('SCHOOL', 253),
 ('STATION', 183),
 ('UNIVERSITY', 152),
 ('PUBLIC', 149),
 ('STEEL', 147),
 ('PLANT', 116),
 ('REPUBLIC', 107),
 ('FIRE', 103),
 ('CHURCH', 92),
 ('LIBRARY', 90),
 ('UIC', 71),
 ('APARTMENTS', 66),
 ('HOSPITAL', 66),
 ('OFFICE', 63),
 ('POST', 63),
 ('TOWER', 63)]
```
The overlapping matrix that we get from the graph is as follows:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/big_words_overlapping1.jpg)

This confirms that 'AIRPORT' and 'OHARE' are synonyms, and both are almost synonyms to
'STRUCTURE'. We find that 'SCHOOL' or 'LIBRARY' have a strong correlation with 'PUBLIC',
that 'STEEL' is highly overlapping with 'PLANT' and 'REPUBLIC', and that
'POST' is correlated with 'OFFICE' non negligibly. It makes sense that these terms appear
together pretty often. So we remove some of these terms to keep only one in each sub-group and
proceed with the following new restricted list of words:
```
['PARK', 'AIRPORT', 'SCHOOL', 'UNIVERSITY', 'STATION',
'PLANT', 'FIRE', 'LIBRARY', 'CHURCH', 'HOSPITAL', 'UIC',
'APARTMENTS', 'TOWER', 'POST', 'CORP', 'PLAZA', 'INC', 'COLLEGE', 'HOTEL', 'PARKING',
'FACILITY', 'CTA', 'MEDICAL', 'LAKE', 'GAS', 'CEMETERY', 'SHORE', 'STRUCTURES', 'IIT',
'POLICE', 'PLACE', 'CARBON', 'FILLING', 'LAKES', 'BANK', 'STATE', 'PUMP', 'CHEMICAL',
'FIELD', 'GARAGE', 'RVS', 'COMMONWEALTH', 'CLUB', 'INSTITUTE', 'WATER', 'CPS',
'AUTO', 'CAMPUS', 'DRIVE', 'SHOP', 'SQUARE', 'FAA', 'MAINTENANCE', 'BRANCH', 'MUSEUM',
'HEALTH', 'RESIDENCE', 'TOWERS', 'UNIV', 'FUEL', 'PUMPING', 'RIVER', 'SHELTER', 'COLLEGES',
'THEATER', 'REPAIR', 'SERVICE', 'SCIENCE', 'COURT', 'FEDERAL', 'SUITES', 'UNION', 'CORPORATE',
'CONCOURSE', 'TERMINAL', 'CATHOLIC', 'COMMUNITY', 'STORAGE', 'TERRACE', 'HOUSING', 'SERVICES',
'AUTHORITY', 'SANITATION', 'COMPANY', 'ADMINISTRATION', 'SHOPPING', 'ACADEMY', 'STREETS', 'LOFTS',
'COOK', 'RESEARCH', 'FARM', 'AIR', 'BAPTIST', 'CAR', 'CONDOMINIUMS', 'ART', 'INN',
'WAREHOUSE', 'ATHLETIC', 'RESTAURANT', 'MEMORIAL', 'COVENANT', 'SUBSTATION', 'CARGO', 'ANNEX',
'INTERNATIONAL', 'TRAINING', 'NWU', 'HOLY', 'OUR', 'LOCALIZER', 'POWER', 'COMMONS', 'LAKESHORE',
'LAKEVIEW', 'LAW', 'FILTRATION', 'AIRLINES', 'TRANSIT', 'GARDENS', 'SCIENCES', 'GARDEN', 'VEHICLE',
'PEOPLES', 'OFFICES', 'OPERATIONS', 'RESIDENCES', 'CONDOS', 'AUDITORIUM', 'ARFF', 'HANGAR', 'CONVENT',
'CHAPEL', 'METRA', 'SUPPLY', 'TECH', 'PARISH', 'LECTURE', 'ATS', 'TRUST', 'PRIVATE', 'GROUND',
'STATION/STORE', 'MISC', 'CENTRAL', 'WRIGHT', 'RENTAL', 'TEMPLE']
```
giving us the following new list of most frequent words:
```
[('AIRPORT', 303),
 ('PARK', 294),
 ('SCHOOL', 253),
 ('STATION', 183),
 ('UNIVERSITY', 152),
 ('PLANT', 116),
 ('FIRE', 103),
 ('CHURCH', 92),
 ('LIBRARY', 90),
 ('UIC', 71),
 ('APARTMENTS', 66),
 ('HOSPITAL', 66),
 ('POST', 63),
 ('TOWER', 63),
 ('CORP', 57),
 ('INC', 56),
 ('COLLEGE', 55),
 ('PLAZA', 52),
 ('HOTEL', 49),
 ('PARKING', 48)]
```

the corresponding graph:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/graph_shell_rest2.jpg)

and the overlapping matrix:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/big_words_overlapping2.jpg)

The imbalance between words has been significantly reduced, which is characterized
by smaller numbers (and more scattered overlaps) in the overlapping matrix, as well as
much more edges having similar width in the graph (co-occurence network).

As it can be understood from the two graphs, the method here consisted in suppressing
some specific words in order to cut the strongest links between correlated words.
This guarantees that words which are kept (at least for the most frequent words of
interest here) have a reasonably small overlap between each other.
We can now leverage these results by creating a dictionary of GeoDataFrames extracted
from the initial GeoDataFrame and for which buildings have no overlap (meaning they belong
to only one category), and the order chosen to group them is the order of the last
list shown above. Using simple plotting already tested above, we get the following
(awesome!) map of the 20th most popular types of buildings in Chicago:

![](https://fabien-nugier.gitlab.io/blog/post/blog7/chicago_special_buildings_selection.jpg)

We can observe on this map that some facilities are well scattered over the city, but others
are much more localized to some sub-areas of the city. For example, the airport buildings dominate
the top left, parking lots and hotels dominate the city center, just on the East of UIC buildings. Parks
are also clearly visible in some places, as well as other universities and colleges. This proves the great
power of geospatial analysis with GeoPandas and the quality of the provided data.


# Conclusions

I have presented here some geospatial analysis with GeoPandas based on a large dataset
of buildings footprints in Chicago. At first this post was intended to be short, but
the richness of possibilities extended the development of this short project. Even more
maps that those presented have been created during this captivating project.

It is very interesting to see that a dataset like this one already allows to study the
global structure of a city, access its weaknesses and strengths, and carry out some
analysis of buildings by categories. Obviously, the complexity of information, especially
contained in the non-structured data of buildings names, offers the possibility for
other analyses and more insight could be extracted from this data. It is also enlightening
to see that a simple feature like the year of build of buildings can actually reveal
the presence of great social and political events such as migrations and financial crises.
Finally, it was nice to see again the usefulness of graphs to understand complex
relationships between words used in the buildings names and to discover they even have
a specific name.

Graph's usefulness was already illustrated in my [first blog post](https://fabien-nugier.gitlab.io/blog/post/2021-09-26-blog1-world-territories-graph/)
and they will definitely come back in future posts. Similar studies could be done based on
other datasets. For example, very interesting data can be found on the following websites:
- [Open Buildings](https://sites.research.google/open-buildings/#download)
- [OpenStreetMap Data Extracts](http://download.geofabrik.de/)

Probably many others datasets could be exploited to extract interesting geospatial
information, build up principles for city development and solve problems related
to infrastructures, social and investments problems. For the reader interested to learn
more about cities, their historical developments and structure, I strongly advise the two
YouTube videos [Why Cities Are Where They Are](https://www.youtube.com/watch?v=3PWWtqfwacQ)
and [Urban Geography Lecture Video](https://www.youtube.com/watch?v=q8Ox7YVJNPg).
