---
title: Jean-Marc Jancovici at Mines Paris Tech, 2019 - Part II
subtitle: Courses 3 & 4
date: 1921-10-17
tags: ["online course", "CO2 emissions", "climate change", "greenhouse effect"]
---

[**Date:** 2021-10-15]  

> In this series of notes, I will try to summarize some of the key takeaways from the
course of Jean-Marc Jancovici on energy and climate change. I had the occasion to
listen to a talk of Mr Jancovici at the Ecole Normale Supérieur many years ago,
he is an expert on energy and climate change, he is now also a columnist on the famous
French radio RTL, among many other responsibilities. For more information, please visit
his website: [jancovici.com](https://jancovici.com/en/).
>
> Many plots are used in the slides of the course. Obviously, a graph is often a
better summary than a thousand words. Nevertheless, to avoid any problem with copyrights
and to keep the level less technical, I will avoid using many plots shown in the course
and limit the technical material. In addition, these notes are not a full exhaustive
coverage of the course, but are more a subset of the course for which I have though
that taking notes could be interesting for the reader and myself.
For example, the course being held in France, it contains numerous references to the
French energetic mix, for which not every fact is reported.
>
> _**Disclaimer:**_ most of the post content reflect statements made in the course and
not my own belief (even when my own belief may be aligned with the content).
Since the results presented here are mainly taken from the course itself and not produced
by my own analysis, this text should not be seen as the result of my work,
but rather a report on the course made through my own filter, focusing on what I believe
to be more relevant for this blog.
>
> **You can find all the videos and slides of the courses, including the ones that will be
covered in future posts:**
> - [videos of the courses](https://www.youtube.com/results?search_query=cours+Jancovici)
> - [slides of the courses](https://cloud.mines-paristech.fr/index.php/s/nCzdipz0uhIlTlw).


# COURSE 3 - What climate for tomorrow (and after)?

**Video link (in French):** [video course 3](https://www.youtube.com/watch?v=l8pdSClyRds)


November 2021 will see the 26th Conference of the Parties (COP 26) taking place in
Glasgow. Many people say that this COP will be crucial for the future of our planet.
On December 12th, 2015, the COP 21 lead to the so-called
[Paris Agreement](https://en.wikipedia.org/wiki/Paris_Agreement),
stating that countries agree to _"holding the increase in the global average
temperature to well below 2°C above pre-industrial levels and pursuing efforts to
limit the temperature increase to 1.5°C."_. This agreement was left by the USA
in 2017 under president Trump, and later rejoined in 2021 with president Biden.
Despite great successes made since the first COP in 1995, or COP 3 in 1997 where
the [Kyoto Protocol](https://en.wikipedia.org/wiki/Kyoto_Protocol) was adopted,
the pre-industrial levels of CO2 concentration in the atmosphere corresponding to
roughly 280 ppm was already reaching 360 ppm in 1995 and is now above 412 ppm.
In other words, as we all realize every New Year, it is much easier to (sincerely)
take good resolutions than actually implementing them through actions (which is
much less comfortable in practice).

One serious problem with climate change is our incapacity to feel it since it is a
change happening slowly at the scale of human life (though less and less slowly,
and undoubtedly fast at the scale of life on Earth). Another problem is that it is a
problem that manifests itself through averages, and there is a hard-encoded tendency
in humans to notice extreme events rather than average effects. Our perception is
thus biasing us towards inaction (a little like a smoker saying every day he will quit
the next day). For that reason, one should not get confused between "weather" and
"climate", since weather is about a set of physical quantities such as temperatures,
precipitation, wind, cloud coverage, etc. on local spatio-temporal scales (about a week),
while climate addresses long-term dynamics and larger scales (over years or centuries).

Notwithstanding the strong disturbance imposed by humans on the Earth climate, it would
be naive to believe that climate has been stable for all ages before us. Climate is not
in a static state, but a constantly evolving one. Indeed, many sources of variation impose
forcing on the Earth's climate and have a certain range of characteristic times, as
illustrated below (in French):

| ![Factors influencing climate and time scales](https://fabien-nugier.gitlab.io/blog/post/blog4/bard_climate_influencers.jpg) |
|:--:|
| <b>Factors influencing climate and time scales. Source: jancovici.com, based on
Edouard Bard's seminar at the Collège de France.</b>|

We notice from this chart that astronomical events (including events caused by the
structure of the Solar System) and geological dynamics play a significant role on
the Earth's climate on long time scales, typically above 100,000 years.
It is a well known fact that inclination of the Earth had a significant role in the
temperatures at its surface, and that it relates to ice ages.
At shorter time scales, interactions between the atmosphere and oceans play a
strong role. Nowadays, humans play a significant role on the Earth's climate,
this since only about few hundred years.

There are 3 main parameters of astronomical forcing that relate to the numerous
planets present in the Solar System:
- the Earth's orbit around the Sun is a quasi-ellipse with an eccentricity e that
varies with a period of 100,000 years between 0 and 0.06. At present, e ≃ 0.01671.
- the Earth's axis of rotation is tilted with respect to its orbital axis
(the axis perpendicular to its orbital place), with a tilt at present around
23.5° (called obliquity), and this axis of rotation does one rotation every 21,000
years (called the precession of the equinoxes). This tilt is the reason behind
the existence of seasons on Earth.
- the obliquity itself changes over time, with a period of about 40,000 years.

When the obliquity of the Earth is small, ice can remain present at lower latitudes
and we have ice ages taking place. The Earth climate is a subtle equilibrium between
the incoming radiative flux coming from the Sun, and the outgoing flux re-emitted by
ground and clouds. This difference in fluxes creates the so-called greenhouse effect.
It is important to keep in mind that greenhouse effect (mainly due to water vapor
and CO2) is necessary for life on Earth as it has raised the temperature at the
surface of the Earth by roughly 30°C, which was enough to keep water liquid and allow
life to exist. However, the accumulation of greenhouse gases in the atmosphere,
in particular gases characterized by a strong absorption capacity in the far infrared,
is the source of global warming that poses a serious threat to life on Earth.

The increase in greenhouse gases concentration in the atmosphere leads to the
increase of temperatures at low altitudes and a decrease of temperature in the
stratosphere. Having a bigger temperature gradient between the two makes the power
of convection higher, hence leading to stronger storms. Obviously, understanding
climate is a very complex endeavor involving numerous scientific disciplines such
as astrophysics, atmospheric physics and chemistry, oceanography, glaciology,
vulcanology, geoscience, biogeochemistry, biology, human sciences, etc.
What we call climatology is a grouping of many experts in this field, such as the
Intergovernmental Panel on Climate Change (IPCC).

A few dates:
- **1824**: Joseph Fourier discovers the greenhouse effect.
- **1838**: <p>Claude Pouillet and Joseph Tyndall understand that greenhouse effect is due
to water vapor and CO2.</p>
- **1896**: Svante Arrhenius predicts that the use of fossil fuels will induce global
warming, estimating an increase of around 4°C if the amount of CO2 doubles.
He also predicted that the lower the temperature of the environment, the higher the
heat transfer. Hence, global warming increases temperatures faster at high latitudes
rather than low ones, at night rather than during the day, and in winter rather than
summer.
- **1922**: Lewis Fry Richardson attempts the first climate simulation.

**_Note 1:_** The statement regarding the stronger warming of cold environments is
based on the Stefan-Boltzmann law that describes energy transfer through
electromagnetic radiation. The net radiated power P of a gas is given by:
$$
P = e \sigma A (T^4 - T_c^4)
$$
with:
- A: the radiating area (in $m^2$).
- σ: Stefan's constant, σ = 5.6703 10<sup>-8</sup> W / m<sup>2</sup> K<sup>4</sup>.
- e: emissivity (=1 for an ideal radiator).
- T: temperature of radiator (in Kelvin degrees K).
- T<sub>c</sub>: temperature of surroundings.
Hence, we understand that heat radiation emitted by CO2 increases the temperature
of cold air in the way described above.

Other greenhouse gases, excluding water (55% as vapor and 17% as clouds) and
CO<sub>2</sub>, include methane CH<sub>4</sub>, nitrous oxide N<sub>2</sub>O,
complex molecules such as C<sub>x</sub>H<sub>y</sub>F<sub>z</sub>Cl<sub>t</sub>,
and Ozone which absorbs a large spectrum from infrared (IR) to ultraviolet (UV)
wavelengths. Only try-atomic molecules can absorb Earth's infrared wavelengths.

Emissions of CO2 mainly come from deforestation, cements production and use of fossil
fuels, as illustrated bellow:

| ![CO2 emissions by energy sources.](https://fabien-nugier.gitlab.io/blog/post/blog4/co2_sources.jpg) |
|:--:|
| <b>CO2 emissions by energy sources. Source: jancovici.com.
Y-axis represents quantities of emitted CO2 in billion tons.</b>|

Lime calcination refers to the emission of CO2 during the production of cements.
Tracking CO2 emissions by regions of the world, we find the following assessment:

| ![CO2 emissions by emitting regions.](https://fabien-nugier.gitlab.io/blog/post/blog4/co2_countries.jpg) |
|:--:|
| <b>CO2 emissions by emitting regions. Source: jancovici.com.</b>|

One can see on this plot that only two events (1932 with the great depression and 1945
after the WWII) led to a decrease of global emission by more than 4%. In order to reach
the objective set by the Paris agreement, emissions also need to get reduced by 4%
every year. This gives an appreciation of the efforts, one way or another, that the
world needs to go through in order to reach these objectives.

Politics often state that it is possible to keep economic growth and reduce CO2
emissions at the same time. This may be true in the future, but far from being the
case at the moment. Indeed, as we have seen above, most of CO2 emissions come from
the use of fossil fuels and, at the same time, most of economic growth of modern
times come from the use of oil and gas. It is not a surprise then to see that GDP
and CO2 emissions have been strongly correlated over time:

| ![Global GDP and CO2 emissions](https://fabien-nugier.gitlab.io/blog/post/blog4/gdp_co2.jpg) |
|:--:|
| <b>Global GDP and CO2 emissions. Source: jancovici.com.</b>|



Obviously, innovations and clean sources of energies does also create points of GDP.
It is the fast domination of fossil energies which is at the origin of this strong
correlation. Analyzing CO2 emissions of France over time, one can see that the world
wars have strongly reduced emissions, but the reduction of coal usage, replacement of
oil by gas in industry, and particularly the creation of the French nuclear program
had a stronger reduction effect. This is a great example of possible economic growth
without CO2 emissions.

Deforestation, which is also a significant source of emissions, has varied from
region to region over time. Two factors essentially lead this behavior, growth in
population and meat consumption. Nowadays, the main regions undergoing deforestation
are South East Asia and South & Central America.

One can see on the following graph the time evolution of CO2 and O2 in the atmosphere:
| ![Time evolution of CO2 and O2 in the atmosphere](https://fabien-nugier.gitlab.io/blog/post/blog4/co2_o2_time.jpg) |
|:--:|
| <b>Time evolution of CO2 and O2 in the atmosphere (X-axis missing, but each period
corresponds to a year). Source: Not certain (IPPP mentioned in the slides, but not
found in their documents). </b>|

In this plot, oscillations with an annual periodicity come from the fact that most
continents of the Earth belong to the Northern Hemisphere, creating an imbalance of
CO2 absorption during spring and emission during autumn, both being stronger in the
North Hemisphere compared to the Southern Hemisphere. One can also see that quantities
of O2 in the atmosphere decrease over time, which is a proof that the overall increase
of CO2 is due to the oxidation of carbon-based molecules. Furthermore, volcanic activity
has a minor impact compared to human emissions.

It is important to remember that oxides are very stable molecules, and this is also
the case for CO2. Hence, when CO2 ends up into the atmosphere, there is no chemical
process to remove it (contrary to methane CH4). Two processes contribute to the 
reduction of CO2 though:
- the absorption of CO2 by the oceans (a pressure equilibrium effect),
- and photosynthesis from plants (endothermic reaction requiring light).

This makes CO2 accumulate into the atmosphere and stay there for a long period of time.
Typically, after a hundred years still remains more than 40% of the initial amount of
CO2, a thousand years after 20% still remains, and 10,000 years after 10% still remains.
As for other greenhouse gases, they all have different life times in the atmosphere.
Among the longest lifetime gases, one can mention HFC, CFC (now forbidden), CF4 and SF6.

Analyzing ice cores from Antarctica, one can see that CO2 concentration stayed
between 180 ppm and 280 ppm over the last 650,000 years, while it is now around
412 ppm and moving up exponentially since industrial revolution. For CH4, it
fluctuated between 300 and 700 ppl, while it is now around 1890 ppb. Finally, the
concentration of N2O fluctuated between 200 and 270 pub, it is now around 330 ppb.
Obviously, human activities have a strong disturbing impact on the concentration of
greenhouse gases.

The main sources of CH4 emissions are the following:

| ![Main emitters of CH4](https://fabien-nugier.gitlab.io/blog/post/blog4/ch4_sources.jpg) |
|:--:|
| <b>Main emitters of CH4. Source: jancovici.com.</b>||:--:|
| <b>Greenhouse gas emissions in CO2 equivalent per capita (globally).
Source: jancovici.com.</b>|

Combining CO2 with methane, we get the following emissions of greenhouse gases in
CO2 equivalent:

| ![Greenhouse gas emissions in CO2 equivalent](https://fabien-nugier.gitlab.io/blog/post/blog4/co2_ch4_sources.jpg) |
|:--:|
| <b>Greenhouse gas emissions in CO2 equivalent. Source: jancovici.com.</b>|

Now accounting for the evolution of human population, we get the following emissions
of CO2 equivalent per capita:

| ![Greenhouse gas emissions in CO2 equivalent per capita (globally)](https://fabien-nugier.gitlab.io/blog/post/blog4/co2_ch4_sources_percapita.jpg) |


Among the other gases mentioned above, two of them stand out particularly.
One of them is N2O which is mainly related to farming activities through pesticides.
The second one is ozone, which plays a fundamental role in the Earth's climate.
Indeed, through its absorption of high energy UVs, ozone protects life on Earth.
Furthermore, through radiative re-emission ozone heats up the troposphere, inverting
the temperature curve (which is supposed to decrease with decreasing pressure, and 
so increasing altitude), and this has a fundamental role in limiting clouds to the 
troposphere. Without such an effect, water vapor could reach much higher altitudes, 
being exposed to hard UVs and dissociating H2O into O2 and H2, with H2 that would 
have escaped into space because of its light weight. Hence, ozone allowed the 
formation of a stable atmosphere and is fundamental for the existence of life
on Earth.

Roughly speaking, global emissions in 2017 were divided in the following amounts:
- 20% from coal power plants
- 7% from gaz and oil power plants
- 6% from cements.
- 11% from industry.
- 5% from buildings.
- 14% from transportations (cars 6%, trucks 4%, boats 2%, planes 2%).
- 20% from farming.
- 10% from deforestation.
- 7% from others.

Obviously, these numbers vary depending on countries and depending on how emissions
are counted. Furthermore, when looking at data for a given country, one should keep 
in mind that emissions may not be representative when countries import goods from
other countries (in which CO2 and other gases are emitted).

Finally, let us note that physical effects can also lead the global temperatures to
decrease, as show in the following plots:

| ![Forcings used in simulations](https://fabien-nugier.gitlab.io/blog/post/blog4/global_forcings.jpg) |
|:--:|
| <b>Forcings used in simulations. Source: Hansen metal, Science 2005.
[Link](https://pubs.giss.nasa.gov/docs/2005/2005_Hansen_ha00110y.pdf)</b>|

<br>

# COURSE 4 - What climate for tomorrow (bis)?

**Video link (in French):** [video course 4](https://www.youtube.com/watch?v=JKoRsO5fkAQ)

Another important type of human emissions is aerosols and aerosol precursors. Aerosols
are made of thin solid or liquid particles of one or more chemical substances present
in a gas. They can lead to the decrease of temperatures on short timescales. They can
also trigger the formation of clouds, and cloud formation is actually slightly changed
over industrial regions. Aerosols all have short lifetimes in the atmosphere.

The following graph shows the effect of different substances on the Earth atmosphere.
We can see that the total anthropic contribution is about 3 W / m^2:

| ![Aerosols and other greenhouse gases effects on radiative forcing.](https://fabien-nugier.gitlab.io/blog/post/blog4/aerosols_giec2014.jpg) |
|:--:|
| <b>Aerosols and other greenhouse gases effects on radiative forcing.
Source: jancovici.com. Based on IPCC 2014 report.</b>|


**_Note 2:_** To get a reference for the plot above, the solar flux density reaching the
Earth is 1361 W / m^2 at one astronomical unit (A.U.) from the Sun. This means the 
energy received by the cross-section of the Earth at its distance from the Sun. Since
the surface of a disk is $\pi R^2$ and that a sphere with the same radius has a half
surface of $4 \pi R^2 \times 1/2$, we get that the hemisphere exposed to the Sun
receives (1361 / 2) W / m^2, and since we need to average between days and nights over
time, this quantity must be divided by two again. This leads to an average flux of 
1361 / 4 = 340 W / m^2 at the top of the atmosphere. The Earth being covered partially
by clouds and considering their albedo, we have to remove 30% of the incoming flux
(as mentioned in the course), and we reach the value of 238 W / m^2 on average at the
surface of the Earth. This is very close to the value quoted on the caption of the 
above plot.

**Additional plot:** Consulting 
[Wikipedia](https://en.wikipedia.org/wiki/Earth%27s_energy_budget), we can find the
following description of the Earth's energy budget:

| ![Earth energy budget in details](https://fabien-nugier.gitlab.io/blog/post/blog4/earth_radiation_flux.jpg) |
|:--:|
| <b>Earth energy budget in details. Source: Wikipedia and NASA.</b>|


Simulations employed today are based on economic and demographic assumptions leading
to predictions of emissions. When combined with climate models, they lead to future
climate predictions. However, retro-action on the economic and demographic assumptions
are not taken into account. Since emissions are impossible to predict (and directly
depend on people's choices of consumption and political choices), we cannot predict
accurately the future of emissions. However, we can build accurate scenarii of emissions
and see towards which type of climates they lead. For that, the Earth surface is 
decomposed into cells and the full columns of oceans and atmosphere are decomposed 
with fine precision.

The following pictures shows the evolution of the grid size from the first to the 
fourth IPCC assessment reports. We can clearly see the increase in precisions:

| ![IPCC Assessment Reports simulation grids](https://fabien-nugier.gitlab.io/blog/post/blog4/ipcc_ar4.jpg) |
|:--:|
| <b>IPCC Assessment Reports simulation grids. Source: jancovici.com.
Based on IPCC 4th Assessment Report.</b>|

In parallel with the improvements of computing power, the number of phenomena taken 
into consideration in the simulations has also increased over time:

| ![Evolution of phenomena taken into consideration by IPCC models](https://fabien-nugier.gitlab.io/blog/post/blog4/models_complexity.jpg) |
|:--:|
| <b>Evolution of phenomena taken into consideration by IPCC models.
Order in times of reports is '1970s' -> '1980s' -> FAR -> SAR -> TAR -> AR4.   
Source: jancovici.com, based on IPCC report 2007.</b>|

One important fact to notice is that even though the models are refined all the time, 
simulations are consistent year after year on the global scale predictions. This is 
because the first order of global warming is well described by CO2 emissions. Hence, 
refinements mainly improve local scale predictions. After being formulated, models are
back-tested to evaluate their predictive power.

The following graph shows the prediction of global temperatures under several scenarii,
as described on the 4th Assessment Report of IPCC:

| ![Predictions of the 4th IPCC Assessment Report](https://fabien-nugier.gitlab.io/blog/post/blog4/ar4_predictions.jpg) |
|:--:|
| <b>Predictions of the 4th IPCC Assessment Report. 
Temperature is defined as the difference with respect to the average over the 1980-1999
period. Source: jancovici.com.</b>|


**_Note 3:** As of 2021, the IPCC published its 6th Assessment Report. Results of this
report confirms previous predictions and stresses the urgency to act. Since this work
deserves a blog post on its own, its discussion is left for a future post.

Let us report a remark made during the course regarding the error bars of the plot.
One psychological bias that may affect some people is to consider that a risky 
scenario with a high uncertainty may not be that risky in the end. This is an incorrect
reasoning, especially when dealing with climate, as a risky scenario with high 
uncertainty is actually representative of a very big risk.

Here, what is important to understand is what is the natural variability of the climate
system, as it is important to know how the system is going to react to the forcing
imposed on it. Paleoclimatology is the discipline aiming at reconstructing climate of 
the past and to know the limits of the climatic system.

One technique to reconstruct the climate over the last few thousand years on 
continents is based on tree measurements (c.f. e.f. dendrochronology), and leads to 
interesting results. We know from it that climate was a little warmer during middle 
age (roughly similar to year 2000), and that it was a little cooler around 1800.
Another technique uses oceanic sediments and goes farther than 60 million years in 
the past. One can see from this kind of measurements that climate has been warmer in 
the past, up to even possibly 10°C, but we have no certainty on how the system would
react and how animal populations (including humans) will react with such an abrupt
modification of climate that human activities are forcing on the system. Finally, 
another technique that can be used is based on isotopes of oxygen and hydrogen in ice
cores, bringing information up to 800,000 years into the past.

To take a concrete example, we can consider the change in temperature that happened
over Europe since 20,000 years. At that time, Northern Europe (and all Canada) were
covered by an ice sheet of 3 km thick and the ocean levels were 120 meters lower. 
Southern Europe had a dry and cold landscape, with at best dry steppes resembling
today's North Siberia. Hence, only about 100,000 humans could live in such an
ecosystem. The whole change in landscape (and human populations) that led to Europe
as we know it today is only due to a 5°C increase of temperatures in 5,000 to 10,000
years. Obviously, a few degrees of change happening over a century will have 
dramatic consequences.

Not only a percent more of greenhouse effect leads to roughly one degree Celsius more 
for the atmosphere, the atmosphere itself is only the tip of the iceberg. Indeed, 90%
of the extra solar energy accumulated from greenhouse effect is in fact stored into 
the oceans. Estimates say that oceans stored more than 250 ZJ (Zeta Joules!) since 1970.
Consequently, climate on Earth is strongly dependent on ocean dynamics at large scales.

Some impacts that climate change will have include:
- weakening, disappearance, displacement of ecosystems.
- rise in ocean levels.
- impact on ocean currents and regional climates.
- change in frequency of extreme weather events (heat waves, rains, droughts, etc.).
- widening of the ozone hole.
- impact on human health (propagation of diseases, consequences of events, etc.).
- acidification of oceans.
- geopolitical risks, economic risks, ...

Following simulations, we can estimate the effect of climate change on average over
the period 2081-2100:

| ![Predictions from climate simulations](https://fabien-nugier.gitlab.io/blog/post/blog4/climate_scenarii.jpg) |
|:--:|
| <b>Predictions from climate simulations.
RCP refers to [Representative Concentration Pathway](https://en.wikipedia.org/wiki/Representative_Concentration_Pathway) 
and the number refers to the radiative forcing assumed for year 2100 (in W/m^2).
Source: jancovici.com, based on IPCC 5th Assessment Report, 2014.</b>|

We can see that the North Pole and continents will be the most affected places by
warming. Also, we notice that the Atlantic Ocean near Greenland remains a cold spot.
Obviously, models' predictions have a certain dispersion, and this dispersion is due
to the parametrization of sub-grid physics, a typical example being the 
parametrization of clouds. It is also important to notice that models establish
predictions within a given set of fixed assumptions about the economy, and do not 
account for the back-reaction of climate change on the economy.

Without any possible doubt, temperatures are rising:

| ![Temperatures rising on land and seas](https://fabien-nugier.gitlab.io/blog/post/blog4/temperatures_rising.jpg) |
|:--:|
| <b>Temperatures rising on land and seas. Source: jancovici.com.</b>|

Having shown the predictions of models, one can now look at real measurements.
What we find is the following map:

| ![Real temperature map](https://fabien-nugier.gitlab.io/blog/post/blog4/real_temperature_map.jpg) |
|:--:|
| <b>Real temperature map. Source: jancovici.com, based on IPCC 5th Assessment Report, 2014.</b>|

As we can see, the world average temperatures are following the predictions of IPCC
reports. Furthermore, we know that the distribution of temperatures (that we can think
of as a normal distribution) not only shifts towards higher temperatures, but also 
broadens. This means that the number of very extreme events is likely to increase even
faster than we might think based on a simple shift of temperatures. This increase in
variability also explains that despite global warming, we can still have winters
colder than reference levels (while it is rarely the case for summers). In addition, 
the temperature is rising in the lower troposphere while decreasing in the lower 
stratosphere.

It is not easy to understand local effects of climate change on precipitations and 
warming. What seems to be true across many simulations though is the increase of 
rainfalls around poles and around the equator, while droughts will increase above
tropics, i.e. at intermediate latitudes. In particular, predictions show that the 
most numerous droughts will happen around the Mediterranean Sea. Concerning ecosystems,
including agricultural lands, the most important aspect is not so much the temperature
itself, but the amount of rainfalls. Here is the result of corresponding simulations:

| ![Soil moisture prediction for period 2090-2099 with respect to average over 1980-1999](https://fabien-nugier.gitlab.io/blog/post/blog4/soil_moisture_prediction.jpg) |
|:--:|
| <b>Soil moisture prediction for period 2090-2099 with respect to average over
1980-1999. Source: jancovici.com, based on IPCC 4th Assessment Report, 2014.</b>|

Climate change will have significant impacts on populations of trees, occurrences of
wildfires, migration of species, loss of ecosystems. One can verify that climate 
change is already impacting yield of the main crop types (such as wheat, soy, rice,
maize) by a few percents per decade. In addition, a rise in temperatures can favor 
the emergence of micro-organisms and viruses. It also rises the level of oceans 
through the melting of glaciers and ice sheets, and indirectly through the melting 
of sea ice that warms up the ice shelves. Consequently, every year the arctic sea ice
surface diminishes, and the sea level could rise by several meters in the coming 
centuries. This would have a strong impact on numerous habitations and infrastructures 
related to trade. Not only the sea level is affected, but also the oceanic currents
are threatened. Related to that is the cold spot on the South of Greenland, where
water sees its salinity increase and temperature decrease, plunging at deeper levels.
Ocean circulation could be diminished because of climate change, with strong impact 
on local climates as well as marine fauna. In addition, oceans are getting acidic at
unprecedented speed (potentially 0.5 point in pH over a century), potentially 
destroying coral reefs and plankton.

In combination to these phenomena, extreme weather events should also increase in
numbers and intensity. For example, cyclones and hurricanes are sourced by a convection
effect between the hot surface of the ground and the cold stratosphere, and since the
first is getting hotter and the latter is getting colder, combined with more evaporation
we get a stronger convective pump that should increase their number and intensity.

One should understand that all systems on Earth are related to each other, and that
human emissions are changing the state of equilibrium in which the Earth has been for
thousands of years. The different systems and their interconnections are complex and
difficult to understand. At the moment, the atmosphere conserves about 45% of CO2
emissions (increasing), plants absorb about 30% (stable), and oceans absorbs about 
25% of emissions (decreasing in capacity). We should also mention that ecosystems 
can change behavior and re-emit CO2 under certain conditions, causing a potential 
risk of a climatic runaway. In this category, we can also add the threats of methane
hydrates and permafrost.

As a final reminder, let us stress that because of the chemical stability of CO2, even
the drop in emissions to zero will not make CO2 disappear from the atmosphere for
thousands of years after (unless capture mechanisms are set in place), and so will 
also last average temperatures and impacts on oceans for thousands of years.


<br>

_To be continued..._
