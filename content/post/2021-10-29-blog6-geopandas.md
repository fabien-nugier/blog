---
title: Package review - Geopandas
subtitle: Reviewing commands from the user guide
date: 1921-10-29
tags: ["package review", "data science", "maps", "geospatial", "python"]
---

[**Date:** 2021-10-29]  

<img src="https://fabien-nugier.gitlab.io/blog/post/blog6/geopandas_logo.svg" alt="GeoPandas Logo" width="200"/>

Since it has been a few posts already that I have shown no Python, it is time to go
back to the basics and write down some code!

The goal of this post is to review the most interesting tools from [GeoPandas](https://geopandas.org/index.html)
(according to my view and knowledge), trying to have a large scope by going through as
many functions as possible. GeoPandas is a very useful package to handle geospatial data
in python, and is actively maintained (and hopefully for a long time), with its last version
being GeoPandas 0.10.0 (on October 3rd, 2021). Hence, this post will not be very original
in the sense that it will mainly follow the package's [documentation](https://geopandas.org/docs.html).
However, instead of just taking the same exact examples as the user guide, I will try to
adapt some of them and present semi-original content.

Since space (and time) is limited, I will not describe how to install the package, but
readers can refer to the [installation page](https://geopandas.org/getting_started.html)
of the package. Furthermore, GeoPandas borrows a lot from [Pandas](https://pandas.pydata.org/),
so it is assumed here that the reader is already partially familiar with this well-known
package. Geometric operations of GeoPandas are based on [Shapely](https://github.com/Toblerity/Shapely),
and this package clearly deserves a blog post, but I will focus on GeoPandas instead. 

As a reminder, let me recall that most of this post content is based on the [Introduction
to GeoPandas](https://geopandas.readthedocs.io/en/latest/getting_started/introduction.html), 
the [GeoPandas' User Guide](https://geopandas.readthedocs.io/en/latest/docs/user_guide.html),
and the [Advanced Guide](https://geopandas.readthedocs.io/en/latest/docs/advanced_guide.html)
(which is of limited content). To explore further, the reader to explore the examples
of the [Examples Gallery](https://geopandas.readthedocs.io/en/latest/gallery/index.html),
and consult the [API Reference](https://geopandas.readthedocs.io/en/latest/docs/reference.html)
for more specific questions regarding the use of functions.


# Data Structure

The core data structure of GeoPandas is the class _geopandas.GeoDataFrame_. This class
inherits from the Pandas' class _pandas.DataFrame_ but augments it with geometry columns
handled through the class _geopandas.GeoSeries_ that inherits from _pandas.Series_.
Each GeoSeries can contain different geometry types and has a _GeoSeries.crs_ attribute
to specify its coordinate reference system. However, only one GeoSeries (no less, no more)
is active in a GeoDataFrame, meaning that it will be the one receiving geometric
operations. The geometry types correspond to [Shapely objects](https://shapely.readthedocs.io/en/stable/manual.html):
Points, Multi-Points, Lines, Multi-Lines, Polygons, Multi-Polygons.

Here is an illustration of a GeoDataFrame, directly taken from the GeoPandas' introduction
page:

![](https://geopandas.org/_images/dataframe.svg)


# Loading packages and datasets

The basic packages necessary here are loaded as follows:
```python
# Basics
import numpy as np
import pandas as pd

# Plots
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

# GeoPandas
import geopandas as gpd
gpd.options.display_precision = 3
```
where we have restricted the number of digits to appear in GeoDataFrames to 3.

Geopandas offers 3 datasets whose names can be obtained as follows:
```python
gpd.datasets.available
```
```
['naturalearth_cities', 'naturalearth_lowres', 'nybb']
```

We can load these 3 datasets into GeoDataFrames:
```python
# Loading countries data
path = gpd.datasets.get_path('naturalearth_lowres')
world = gpd.read_file(path)

# Loading cities data
path = gpd.datasets.get_path('naturalearth_cities')
cities = gpd.read_file(path)

# Loading NYC data
path = gpd.datasets.get_path('nybb')
nybb = gpd.read_file(path)
```
and using `.head(3)` on them we can see the first 3 rows of each dataset:

| ![](https://fabien-nugier.gitlab.io/blog/post/blog6/datasets_head.jpg) |
|:--:|
| <b>View on the datasets content.</b>|

As it will be important later, we can also check the CRS attribute of the GeoDataFrame:
```python, color=red
world.crs
```
```
<Geographic 2D CRS: EPSG:4326>
Name: WGS 84
Axis Info [ellipsoidal]:
- Lat[north]: Geodetic latitude (degree)
- Lon[east]: Geodetic longitude (degree)
Area of Use:
- name: World.
- bounds: (-180.0, -90.0, 180.0, 90.0)
Datum: World Geodetic System 1984 ensemble
- Ellipsoid: WGS 84
- Prime Meridian: Greenwich
```
Plotting the countries and cities on the same map can be done as follows:
```python
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
world.plot(ax=ax, color='blue', alpha=0.5)
cities.plot(ax=ax, color='green', alpha=0.5)
plt.title('Countries and Cities datasets')
plt.xlabel('Geodetic longitude (°)')
plt.ylabel('Geodetic latitude (°)')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/datasets_countries_cities.jpg)

We can notice that the CRS for the New York City boroughs dataset is different:
```python
nybb.crs
```
```
<Projected CRS: EPSG:2263>
Name: NAD83 / New York Long Island (ftUS)
Axis Info [cartesian]:
- X[east]: Easting (US survey foot)
- Y[north]: Northing (US survey foot)
Area of Use:
- name: United States (USA) - New York - counties of Bronx; Kings; Nassau; New York; Queens; Richmond; Suffolk.
- bounds: (-74.26, 40.47, -71.8, 41.3)
Coordinate Operation:
- name: SPCS83 New York Long Island zone (US Survey feet)
- method: Lambert Conic Conformal (2SP)
Datum: North American Datum 1983
- Ellipsoid: GRS 1980
- Prime Meridian: Greenwich
```
and we obtain its plot in a similar way:
```python
nybb.plot()
plt.title('New York Boroughs dataset')
plt.xlabel('Easting (US survey foot)')
plt.ylabel('Northing (US survey foot)')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/datasets_ny_boroughs.jpg)


# Simple manipulations of GeoDataFrames

We can remove two columns of the NYC Boroughs dataset in the following way:
```python
nybb.drop(columns=['Shape_Area', 'Shape_Leng'], inplace=True)
```
and rebuild them, among other quantities extracted from the multipolygons contained in
the 'geometry' column:
```python
# Use the BoroCode column as an index
nybb.set_index('BoroCode', inplace=True)

# Compute the area of each borough
nybb['area'] = nybb.area

# Compute the centroids of each borough
nybb['centroid'] = nybb.centroid

# Compute the boroughs boundaries shapes
nybb['boundary'] = nybb.boundary

# Compute the length of each borough
nybb['boundary_lenth'] = nybb.boundary.length

nybb
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/datasets_ny_boroughs_more.jpg)

and we could check easily that the quantities recomputed for area and length of the
boroughs match with the previous ones contained in the initial dataset.

Let us now focus on the world map and show that computing centroids can be more subtle
than we think. Indeed, on a flat surface we have a unique centroid for a given
shape on which all subpart has the same weight. However, we are dealing here with the
Earth surface through a coordinate system, and so our centroids depend on the chosen
projection for the GeoDataFrame.

So when running the following code:
```python
world['centroid_naive'] = world.geometry.centroid
```
we add a new column of computed centroids to the GeoDataFrame, but we also get the
following warning:
![](https://fabien-nugier.gitlab.io/blog/post/blog6/centroid_warning.jpg)

This warning makes sense since the current CRS is WGS 84, as displayed above, and this
system of coordinates does not conserve areas. For that reason we first project the
countries on the Equal Area Cylindrical coordinates, which are equivalent to a flat surface,
and compute the centroid there. Then we simply project back to the original coordinates.
This is done as simply as follows:
```python
world['centroid'] = world.geometry.to_crs('+proj=cea').centroid.to_crs(world.crs)
```
To learn more about this subject, please refer to that post on [gis.stackexchange.com](https://gis.stackexchange.com/questions/372564/userwarning-when-trying-to-get-centroid-from-a-polygon-geopandas).

Just to prove that this kind of issues is not just perfectionistic views, let us plot
the world map with the "naive centroids" and the correct centroids:
```python
fig, ax = plt.subplots(figsize=(10, 10))
world['geometry'].plot(ax=ax, color='gray', alpha=0.5)
world['centroid'].plot(ax=ax, color='green', alpha=0.5)
world['centroid_naive'].plot(ax=ax, color='blue', alpha=0.5)
plt.title('World centroids (naive and correct)')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/naive_vs_correct_centroids.jpg)

Due to the initial coordinate system (WGS 84), the centroids which we badly computed
were those present at high (absolute) latitudes.

We can create two GeoDataFrames, one for each Earth Hemisphere, based on their
centroids:
```python
# Select data
northern_hemisphere = world[world['centroid'].y > 0]
southern_hemisphere = world[world['centroid'].y < 0]

# Plot
fig, ax = plt.subplots(figsize=(10, 10))
northern_hemisphere.plot(ax=ax, color='green')
southern_hemisphere.plot(ax=ax, color='blue')
ax.axhline(y=0, color='gray', linestyle='--')
plt.title('World map with separation of hemispheres')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_hemispheres.jpg)

Let's now do a bit of renaming and dropping some columns:
```python
# Renaming a simple column
northern_hemisphere = northern_hemisphere.rename(columns={'pop_est': 'population'})

# Renaming a column which holds the geometry property
northern_hemisphere = northern_hemisphere.rename(columns={'geometry': 'country_shape'})
northern_hemisphere = northern_hemisphere.set_geometry('country_shape')

# Removing the other geometry columns (otherwise file can't be saved)
northern_hemisphere.drop(columns=['centroid', 'centroid_naive'], inplace=True)
```
we can now save our Northern Hemisphere dataset as follows:
```python
path = './blog6/northern_hemisphere.geojson'
northern_hemisphere.to_file(path, driver='GeoJSON')
```
and reload it as follows:
```python
test = gpd.read_file('./blog6/northern_hemisphere.geojson')
```

Many other formats can be used to save GeoDataFrames. We have shown above an example
with a GeoJSON file, we could also use a shapefile:
```python
path = './blog6/northern_hemisphere.shp'
northern_hemisphere.to_file(path)
```
or we can even save several datasets under the same GeoPackage file. For example,
we can combine countries and cities as follows:
```python
path = './blog6/countries_and_cities.gpkg'
world.drop(columns=['centroid_naive', 'centroid']).to_file(path, layer='countries', driver='GPKG')
cities.to_file(path, layer='cities', driver='GPKG')
```
and recover them as follows:
```python
path = './blog6/countries_and_cities.gpkg'
test_countries = gpd.read_file(path, layer='countries')
test_cities = gpd.read_file(path, layer='cities')
```

The separation of hemispheres that we have shown above can also be done quickly
through the use of so-called "cx indexer" `.cx()` which selects countries directly
from their coordinates:
```python
fast_southern_hemisphere = world.cx[:, :0] # So here we use .cx[longitudes, latitudes]
fast_southern_hemisphere.plot()
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/southern_from_cx.jpg)
As we can notice, the separation of countries is different from the last, this is due
to the indexer removing any country whose territory crosses the equator (while our
above approach was based on the centroids).

Finally, one should be aware that GeoDataFrames can be joined with `.join()` like
Pandas DataFrames, but also with two specific methods called `.sjoin()` and
`.sjoin_nearest()` to operate a join of tables from physical coordinates.
Here is an illustration:
```python
# Reshape world dataset into countries
countries = world[['name', 'geometry']]
countries = countries.rename(columns={'name': 'country'})

# Do an sjoin between countries dataset and cities dataset
cities_with_country = cities.sjoin(countries, how='inner', predicate='within')
cities_with_country
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/countries_cities_sjoin.jpg)


# Making maps

Let us now do some real maps and learn a bit more. Since we have countries' population
as well as their GDP, we can make a map of their GDP per capita. For consistency, let
us remove Antarctica before the plot:
```python
# Select countries with a population and remove Antarctica
world_nosouthpole = world[world.name != 'Antarctica'].copy()

# Compute the GDP per capita
world_nosouthpole['gdp_per_capita'] = world_nosouthpole.gdp_md_est / world_nosouthpole.pop_est

# Plot
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
world_nosouthpole.plot(column='gdp_per_capita', cmap='OrRd', ax=ax, legend=True, cax=cax)
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_gdp_per_capita.jpg)
a minor glitch in this plot is the absence of factor 10^9 (refering to USD Million per person)
which should appear on top of the color bar.

**Note:** if you don't like this color palette, please visit [Matplotlib's colormap page](https://matplotlib.org/stable/tutorials/colors/colormaps.html)
where you will find all the colors that suit your tastes.

Changing the units so that we have thousands of dollars per person in our dataset:
```python
world_nosouthpole['gdp_per_capita'] = world_nosouthpole['gdp_per_capita'] * 1000.
```
we can create a map based on percentiles:
```python
fig, ax = plt.subplots(1, 1, figsize=(10, 10))
world_nosouthpole.plot(column='gdp_per_capita', cmap='OrRd', ax=ax, scheme='percentiles',
                       legend=True, legend_kwds={'loc': 'lower left'})
plt.title('GDP per capita of world countries (excluding Antarctica), in US$ Thousands / Person')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_gdp_per_capita_percentiles.jpg)

Going back to the NYC boroughs dataset, we can compute the convex hull of each borough
and plot them:
```python
# Computing the convex hull of the boroughs
nybb['convex_hull'] = nybb.convex_hull

# Plotting them
ax = nybb['convex_hull'].plot(alpha=0.5, color='gray')
nybb['boundary'].plot(ax=ax, color='red', lw=0.5)
plt.title('NY boroughs with convex hulls')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/ny_boroughs_convexhull.jpg)

Similarly, we can add a buffer of 10,000 feet (the unit of the dataset's CRS):
```python
# Create a buffer around the boroughs
nybb['buffered'] = nybb.buffer(10000)

# Plotting them
ax = nybb['buffered'].plot(alpha=0.5, color='gray')
nybb['boundary'].plot(ax=ax, color='red', lw=0.5)
plt.title('NY boroughs with buffer of 10,000 feet')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/ny_boroughs_buffered.jpg)

Inspired by this plot, and consulting the following [article](https://www.boell.de/en/2017/05/30/ocean-governance-who-owns-ocean),
we can try to approximate the map of countries' Exclusive Economic Zones (EEZ) and high seas
areas. Indeed, the EEZ is defined by a buffer of 200 sea miles, and the following code
provides an acceptable approximation:
```python
# Define a distance (1 sea mile = 1,842 m at the equator)
sea_mile_to_degree = (1.842 / 6371) * (180 / np.pi) # To have a distance in degrees
dist_eez = 200 * sea_mile_to_degree

# Change the geometry back to the countries' polygons
world.set_geometry('geometry', inplace=True)
world['boundary'] = world.geometry.boundary

# Create the buffered polygons for the Exclusive Economic Zone (EEZ)
world['buffered'] = world.buffer(dist_eez)

# Build the map in a nice way
fig, ax = plt.subplots(figsize=(15, 10))
world['buffered'].plot(ax=ax, alpha=0.5, color='cyan')
world['geometry'].plot(ax=ax, color='goldenrod', lw=0.5)
world['geometry'].boundary.plot(ax=ax, color='white', lw=0.5)
plt.title('World map with a buffer of 200 sea miles.')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_eez2.jpg)

One should not be fooled by this map and see that we haven't reprojected the map into
the CEA coordinates system first. Doing so leads to problems due to the coordinate system
and it is not that important for such a quick attempt, results are only biased at higher
latitudes.

Last, but not least, we can use the Folium package in order to do interactive maps:
- of the NYC boroughs dataset:
```python
import folium

map_nyc = nybb.explore(column='area', legend=False)
map_nyc
```
| ![](https://fabien-nugier.gitlab.io/blog/post/blog6/map_folium_ny_borough.jpg) |
|:--:|
| <b>Interactive map [HERE](https://fabien-nugier.gitlab.io/blog/post/blog6/map_folium_ny_borough.html)</b>|
<br>

- of the world countries dataset:
```python
map_world = world.explore(column="pop_est", scheme="naturalbreaks", legend=True, k=10,
                  legend_kwds=dict(colorbar=False), name="countries")

cities.explore(m=map_world, color="red", marker_kwds=dict(radius=10, fill=True), tooltip="name",
               tooltip_kwds=dict(labels=False), name="cities")

folium.TileLayer('Stamen Toner', control=True).add_to(map_world)
folium.LayerControl().add_to(map_world)
map_world
```
| ![](https://fabien-nugier.gitlab.io/blog/post/blog6/map_folium_world.jpg) |
|:--:|
| <b>Interactive map [HERE](https://fabien-nugier.gitlab.io/blog/post/blog6/map_folium_world.html)</b>|


# Geometric manipulations

After all these sweaty efforts to draw some awesome maps, let us do a few simple things,
which are still very useful and directly borrowing from the Shapely package.

Let us build a GeoDataFrame from scratch:
```python
from geopandas import GeoSeries
from shapely.geometry import Polygon

p1 = Polygon([(0, 0), (1, 0), (1, 1), (0, 1)])
p2 = Polygon([(0, 0), (1, 0), (1, 1)])
p3 = Polygon([(2, 0), (3, 0), (3, 1), (2, 1)])
g = GeoSeries([p1, p2, p3])
```
as it is done on the user guide, and draw the shapes it contains:
```python
color_list = ['orange', 'darkorange', 'gray']
g.plot(color=color_list, edgecolor='k')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/geom1.jpg)

It is interesting to notice that there exists an empty polygon, for example obtained
when taking the intersection of the orange shape with the gray shape:
```python
intersection_orange_gray = g.loc[1].intersection(g.loc[2])
print(intersection_orange_gray)
print(intersection_orange_gray.area)
print(intersection_orange_gray.is_empty)
```
```
POLYGON EMPTY
0.0
True
```

We can compute the areas of these shapes:
```python
g.area
```
```
0    1.0
1    0.5
2    1.0
dtype: float64
```
and we can plot a buffer of size 0.5 around them:
```python
g.buffer(0.5).plot(color=color_list, edgecolor='k')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/geom2.jpg)

By creating a mask, one can select France mainland and Spain as two countries from
the world dataset:
```python
# Create a mask
frsp_mask = [x in ['France', 'Spain'] for x in world['name']]

# Select the corresponding rows
frsp = world[frsp_mask]

# Select France mainland by exploding the MultiPolygon into several Polygons
france_main = frsp[frsp['name'] == 'France']['borders'].explode()[(43, 1)]

# Select Spain
spain = frsp[frsp['name'] == 'Spain']['borders'][132]
```
and we can extract basic information between them:
```python
# France mainland centroid
fr_main_centro = france_main.centroid
print(fr_main_centro)

# Spain centroid
sp_centro = spain.centroid
print(sp_centro)
```
```
POINT (2.339088646029322 46.60646337286316)
POINT (-3.617020602387374 40.34865610622673)
```
```python
# France area in the WGS 84 CRS
print(f'France area: {france_main.area} square degrees')

# France bounds:
fr_bounds = france_main.bounds
print(f'France min long: {fr_bounds[0]} deg ; max long: {fr_bounds[2]} deg')
print(f'France min lat: {fr_bounds[2]} deg ; max lat: {fr_bounds[3]} deg')

# Distance between France mainland and Spain centroids:
print(f'France mainland centroid to Spain centroid: {fr_main_centro.distance(sp_centro)} degrees')

# Mainland and centroid inclusions
print(f"Does France mainland contain France mainland centroid: {france_main.contains(fr_main_centro)}")
print(f"Does France mainland contain Spain centroid: {france_main.contains(sp_centro)}")
```
```
France area: 64.62729041684838 square degrees
France min long: -4.592349819344776 deg ; max long: 8.099278598674744 deg
France min lat: 8.099278598674744 deg ; max lat: 51.14850617126183 deg
France mainland centroid to Spain centroid: 8.639177574598325 degrees
Does France mainland contain France mainland centroid: True
Does France mainland contain Spain centroid: False
```
and we should remember that these methods not only work with Shapely objects,
they also work with GeoSeries!

One can illustrate the two countries:
```python
from shapely.geometry import Polygon, Point

# Countries
fr_boundary = france_main.exterior.xy
sp_boundary = spain.exterior.xy

# Centroids
fr_centroid = france_main.centroid.xy
sp_centroid = spain.centroid.xy

# Border
fr_sp_border = [segment.xy for segment in france_main.intersection(spain)]

# Making a GeoPandas GeoDataFrame from coordinates
fr_sp = gpd.GeoDataFrame(data=None, columns=['name', 'boundary', 'centroid'],
                         geometry='boundary', crs=world.crs)
fr_sp['name'] = ['France', 'Spain']
fr_sp['boundary'] = [Polygon(list(zip(x[0], x[1]))) for x in [fr_boundary, sp_boundary]]
fr_sp['centroid'] = [Point(np.array(x).flatten()) for x in [fr_centroid, sp_centroid]]
fr_sp.set_index(keys='name', inplace=True)

# Convex hulls
fr_ch = fr_sp.loc['France', 'convex_hull']
sp_ch = fr_sp.loc['Spain', 'convex_hull']
intersec_ch = fr_ch.intersection(sp_ch)

# Plot
fr_ch_coords = fr_ch.exterior.xy
sp_ch_coords = sp_ch.exterior.xy

fig = plt.figure(figsize=(10, 10))
plt.plot(*fr_boundary, color='blue')
plt.plot(*fr_ch_coords, color='blue', alpha=0.3)
plt.plot(*sp_boundary, color='red')
plt.plot(*sp_ch_coords, color='red', alpha=0.3)
for segment in fr_sp_border:
    plt.plot(*segment, color='green', lw=3)
plt.plot(*fr_centroid, marker='o', color='blue', markersize=10)
plt.plot(*sp_centroid, marker='o', color='red', markersize=10)
plt.title('Map of France mainland and Spain')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/map_fr_sp.jpg)

Now we can do something funny by shifting France exactly on top of Spain's centroid:
```python
from shapely.affinity import translate

# Compute the needed shift
shift_x = np.array(fr_sp.loc['Spain', 'centroid'].xy)[0][0] - np.array(fr_sp.loc['France', 'centroid'].xy)[0][0]
shift_y = np.array(fr_sp.loc['Spain', 'centroid'].xy)[1][0] - np.array(fr_sp.loc['France', 'centroid'].xy)[1][0]
fr_shift = translate(fr_sp.loc['France', 'boundary'], xoff=shift_x, yoff=shift_y)

# Plot
fr_shift_boundary = fr_shift.exterior.xy
fr_shift_centroid = fr_shift.centroid.xy

fig = plt.figure(figsize=(10, 10))
plt.plot(*fr_shift_boundary, color='blue')
plt.plot(*fr_shift_centroid, marker='o', color='black', markersize=10)
plt.plot(*sp_boundary, color='red')
plt.plot(*sp_centroid, marker='o', color='black', markersize=10)
plt.title("Map of France mainland's shifted on top of Spain")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/map_fr_shift_sp.jpg)

Having this map in our hands, we can build two trivial GeoDataFrames as follows:
```python
fr_sp = fr_sp.append({'boundary': fr_shift,
                      'centroid': fr_shift.centroid,
                      'convex_hull': fr_shift.convex_hull},
                     ignore_index=True)
fr_sp.index = ['France', 'Spain', 'France_shift']
fr_sp.drop(index='France', inplace=True)

# GeoDataFrame of France
gs1 = gpd.GeoSeries([fr_sp.loc['France_shift', 'boundary']])
df1 = gpd.GeoDataFrame({'geometry': gs1, 'df1': [1]})

# GeoDataFrame of Spain
gs2 = gpd.GeoSeries([fr_sp.loc['Spain', 'boundary']])
df2 = gpd.GeoDataFrame({'geometry': gs2, 'df2': [1]})
```
and use them to illustrate the `.overlay()` function:
- to compute their union:
```python
union = df1.overlay(df2, how='union')

fig, ax = plt.subplots(1, 1)
union.plot(cmap='tab10', ax=ax)
plt.title('Union of Spain and Shifted France')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/frshifted_spain_union.jpg)
- to compute their intersection:
```python
intersection = df1.overlay(df2, how='intersection')

fig, ax = plt.subplots(1, 1)
intersection.plot(cmap='tab10', ax=ax)
plt.title('Intersection of Spain and Shifted France')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/frshifted_spain_intersection.jpg)
- to compute their difference:
```python
difference = df1.overlay(df2, how='difference')

fig, ax = plt.subplots(1, 1)
difference.plot(cmap='tab10', ax=ax)
plt.title('Difference of Spain and Shifted France')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/frshifted_spain_difference.jpg)
- to compute their symmetric difference:
```python
symmetric_difference = df1.overlay(df2, how='symmetric_difference')

fig, ax = plt.subplots(1, 1)
symmetric_difference.plot(cmap='tab10', ax=ax)
plt.title('Symmetric difference of Spain and Shifted France')
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/frshifted_spain_sym_diff.jpg)

As a last computation, let us illustrate the role of the `.dissolve()` function by
some computation of the GDP per capita, computed over continents after aggregating the
populations and GDPs of countries composing them:
```python
continents = gpd.GeoDataFrame(world[['continent', 'borders', 'pop_est', 'gdp_md_est']],
                              geometry='borders', crs=world.crs)
continents = continents.dissolve(by='continent', aggfunc='sum')
continents['gdp_per_capita'] = continents['gdp_md_est'] / continents['pop_est']
continents.drop(index=['Antarctica', 'Seven seas (open ocean)'], inplace=True)

fig, ax = plt.subplots(1, 1, figsize=(10, 10))
continents.plot(column='gdp_per_capita', cmap='YlOrRd', ax=ax, legend=True,
                legend_kwds={'label': 'GDP per capita by continent',
                             'orientation': 'horizontal'})
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/continents_gdp_per_capita.jpg)


# Projections and CRS

Let us end this post with the topic of Coordinate Reference Systems (CRS) that has been
neglected until this point. CRSs are fundamental to geospatial analysis and dealing with
them can be quite involved, hence this topic should not be neglected when plotting maps
or dealing with GeoDataFrames.

Let us import the package [PyProj](https://github.com/pyproj4/pyproj) and load the world
dataset again:
```python
import pyproj

path = gpd.datasets.get_path('naturalearth_lowres')
world = gpd.read_file(path)
world = world[(world.name != "Antarctica")]
```
Since this GeoDataFrame is in the World Geodetic System 1984 (WGS 84), we get the following
map in the corresponding CRS:
```python
fig, ax = plt.subplots(1, 1, figsize=(10, 5))
world.plot(ax=ax)
plt.title("World in World Geodetic System 1984")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_WGS84.jpg)

Projecting into the Mercator CRS (EPSG 3395), we get the following mapping format
which is also quite popular:
```python
world2 = world.to_crs(epsg=3395)

ax = world2.plot(figsize=(10, 5))
ax.set_title("World in Mercator projection")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_Mercator.jpg)

We can also define the specifications of a CRS by hand, like the EPSG 2163 system:
```python
epsg_2163_crs = "+proj=laea +lat_0=45 +lon_0=-100 +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
world3 = world.to_crs(crs=epsg_2163_crs)

ax = world3.plot(figsize=(10, 5))
ax.set_title("World in EPSG 2163 projection")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_epsg2163.jpg)

Finally, we can tune this hand-made CRS to center it on France's centroid:
```python
c1 = fr_centroid[0][0]
c2 = fr_centroid[1][0]

fr_centered_crs = f"+proj=laea +lat_0={c2} +lon_0={c1} +x_0=0 +y_0=0 +a=6370997 +b=6370997 +units=m +no_defs"
world4 = world.to_crs(crs=fr_centered_crs)

ax = world4.plot(figsize=(10, 5))
ax.set_title("World in a France centered projection")
plt.show()
```
![](https://fabien-nugier.gitlab.io/blog/post/blog6/world_France_centered.jpg)

For further information on CRS, the reader can consult the [Spatial Reference](https://spatialreference.org/)
page, as well as the [EPSG search engine](http://epsg.io/). To read about some specifc
coordinate systems, I advise this very interesting article on [Geo Awesomeness](https://geoawesomeness.com/best-map-projection/).
Let us add that GeoPandas can accept numerous ways to specify the CRS, and we have only
presented two of them in the above lines of code.


# Conclusions

I have reviewed most of the GeoPandas' package User Guide and illustrated its main features
from examples of the package's documentation as well as personal examples.

GeoPandas is a fantastic tool intensively used for geospatial analysis in Python.
I have already shown its usefulness in some previous posts and I will also show new
applications of this package in the near future.
