# Welcome to The Fabien's Weekly Post!

You are here on the repository of my blog, i.e. the place where I store the material
to build up the HTML pages making up the blog.

You can access my blog posts from the following link:

[GO TO MY BLOG POSTS!](https://fabien-nugier.gitlab.io/blog/post/)

Hope you will enjoy reading the content!

<br>
<br>
<br>

# How this blog was built.

My blog is built from [Hugo] and you can consult the [GitLab Pages examples / hugo]
page to make your own blog. You can also read the following page to set it up:
https://kalikiana.gitlab.io/post/2021-02-12-setup-gitlab-pages-blog-with-hugo/
and can read more about Hugo at Hugo's [documentation][].

It also makes use of GitLab Pages, you can learn more about them at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

[GitLab Pages examples / hugo]: https://gitlab.com/pages/hugo
[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
